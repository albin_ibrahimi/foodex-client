const mongoose = require('mongoose');
const Schema = mongoose.Schema;

let productSchema = new Schema({
    name: {
        type: String
    },
    category: {
        type: String
    },
    image: {
        type: String
    },
    description: {
        type: String
    },
    flag: {
        type: String
    },
    stock: {
        type: Number
    },
    price: {
        type: Number
    },
    isDeleted:{
        type: Boolean
    }

}, {
        collection: 'products'
    })

module.exports = mongoose.model('Product', productSchema)