let mongoose = require('mongoose'),
    express = require('express'),
    router = express.Router();

let product = require('../models/products');



router.route('/').get((req, res) => {
    product.find((error, data) => {
        if (error) {
            return next(error)
        } else {
            res.json(data)
        }
    })
})




module.exports = router;