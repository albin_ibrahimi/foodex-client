import React, { Component } from "react"
import { Redirect } from "react-router";
import app from '../base'
import firebase from "firebase"
import StyledFirebaseAuth from "react-firebaseui/StyledFirebaseAuth"
import { Link } from "react-router-dom";



class Login extends Component {
  state = { isSignedIn: false }
  uiConfig = {
    signInFlow: "popup",
    signInOptions: [
      app.auth.GoogleAuthProvider.PROVIDER_ID,
      app.auth.FacebookAuthProvider.PROVIDER_ID,
      app.auth.TwitterAuthProvider.PROVIDER_ID,
      app.auth.GithubAuthProvider.PROVIDER_ID,
       ],
    callbacks: {
      signInSuccess: () => false
    }
  }

  componentDidMount = () => {
    firebase.auth().onAuthStateChanged(user => {
      this.setState({ isSignedIn: !!user })
      console.log("user", user)
    })
  }

 

  render() {
       if(this.state.isSignedIn){
        return <Redirect to="/" />
       }
    return (
      <div className="container">
          <h1 className="fonti text-center mt-5">Login Page</h1>
          <div className="mb-5 mt-5">
          <StyledFirebaseAuth
            uiConfig={this.uiConfig}
            firebaseAuth={app.auth()}
          />
          <button className="btn btn-danger email"><Link  className="text-white float-left" style={{textDecoration:"none", fontWeight:"600"}} to='/loginwemail'><i class="fa fa-envelope float-left" aria-hidden="true"></i>
        Login with Email</Link></button><br/>
          </div>
      </div>
    )
  }
}

export default Login
