import axios from 'axios';
import  React from 'react';
import {Link} from 'react-router-dom';


const Slider  = ({category}) => {

    const [repos, setRepos] = React.useState([]);
    
 
 
   
   React.useEffect(() => {
       const fetchData = async () => {
         const response = await axios.get('http://localhost:4000/category');
         setRepos(response.data);
     }
     
     fetchData();
   },[category]);
   




        return (
    <div className="row">
    <div className="col-lg-2">
    <h2 className="my-4 ml-5 bookscat"></h2>
    <div className="btn-group ml-5" >
    <button type="button" className="btn btn-dark">Kategoritë</button>
    <button type="button" className="btn btn-dark dropdown-toggle px-3" data-toggle="dropdown" aria-haspopup="true"
    aria-expanded="false"> </button>
    <div className="dropdown-menu" >
    <Link className="text-white btn btn-danger ml-3 mr-3 mb-2 " style={{width:"80%"}} to = {`/products`}>Të gjitha</Link>
    { repos.map(item => (
      <div>
       <Link className="text-white btn btn-danger ml-3 mr-3 mb-2" style={{width:"80%"}} to = {`/category/${item._id}`}>{item.name}</Link>
    
      </div>
    
    ))}
    </div>
    </div>
    </div>
    <div className="col-lg-9 mb-4" > 
        <div id="carouselExampleIndicators" className="carousel slide my-4" data-ride="carousel">
          <ol className="carousel-indicators">
            <li data-target="#carouselExampleIndicators" data-slide-to="0" className="active"></li>
            <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
            <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
          </ol>
          <div className="carousel-inner" role="listbox">
            <div className="carousel-item active">
            <img src={require('../assets/slider1.jpg')} className="d-block img-fluid" alt="First slide"/>
              <div className="carousel-caption d-flex">
              <div className="cpl">
              <h3 className="text-warning mb-2 specialiteti">Specialiteti <span className="text-light">për sot</span></h3>
              
              <h6 className="text-light float-left text-left mb-3">Pastas jane te ndara ne dy <br/>kategori: te thata (pasta secca)<br/> dhe fresh (pasta fresca). </h6>
              <br/>

              <button className="float-left btn btn-outline-warning mb-5">Porosit tani!</button>
              
          </div>
          </div>
            </div>
            <div className="carousel-item">
              <img className="d-block img-fluid" src={require('../assets/slider2.jpg')} alt="Second slide"/>
              <div className="carousel-caption d-flex">
              <div className="cpl">
              <h3 className="text-danger mb-4">Specialiteti <span className="text-white">për sot</span></h3>
              <br />
              <h6 className="text-light float-left text-left mb-3">Bifteku është mish i ndarë në dy <br/>pjesë perreth fibrave të tij<br/>potencialisht përfshin dhe ashtin. </h6>
              <br />

              <button className="float-left btn btn-outline-danger mb-5">Porosit tani!</button>
           
          </div>
          </div>
            </div>
            <div className="carousel-item">
              <img className="d-block img-fluid" src={require('../assets/slider3.jpg')} alt="Third slide"/>
              <div className="carousel-caption d-flex">
              <div className="cpl">
              <h3 className="text-success mb-4">Specialiteti <span className="text-white">për sot</span></h3>
              <br />
              <h6 className="text-light float-left text-left mb-3">Një sallatë kopshti përmban lakër<br/> trangull, qepë , qershi, domate<br/> ullinjë, domate të thara, and feta. </h6>
              <br />
              
              <button className="float-left btn btn-outline-success mb-5">Porosit tani!</button>
          </div>
          </div>
            </div>
            </div>

            <a className="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
            <span className="carousel-control-prev-icon" aria-hidden="true"></span>
            <span className="sr-only">Previous</span>
          </a>

          <a className="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
            <span className="carousel-control-next-icon" aria-hidden="true"></span>
            <span className="sr-only">Next</span>
          </a>

          </div>
    </div>
    </div> )
}

export default Slider;