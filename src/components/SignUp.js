import React, { useCallback, useContext,  useState  } from "react";
import { withRouter, Redirect } from "react-router";
import {app , storage} from "../base";
import { AuthContext } from "../Auth.js";


const SignUp = ({ history} ) => {

  const allInputs = {imgUrl: ''}
    const [imageAsFile, setImageAsFile] = useState('')
    const [imageAsUrl, setImageAsUrl] = useState(allInputs)
	console.log(imageAsFile)
 const handleImageAsFile = (e) => {
      const image = e.target.files[0]
      setImageAsFile(imageFile => (image))
  }
  
  const handleFireBaseUpload = e => {
	  console.log(imageAsUrl.imgUrl)
      e.preventDefault()
    console.log('start of upload')
    // async magic goes here...
    if(imageAsFile === '') {
      console.error(`not an image, the image file is a ${typeof(imageAsFile)}`)
    }
    const uploadTask = storage.ref(`/profile/${imageAsFile.name}`).put(imageAsFile)
    //initiates the firebase side uploading 
    uploadTask.on('state_changed', 
    (snapShot) => {
      //takes a snap shot of the process as it is happening
      console.log(snapShot)
    }, (err) => {
      //catches the errors
      console.log(err)
    }, () => {
      // gets the functions from storage refences the image storage in firebase by the children
      // gets the download url then sets the image from firebase as the value for the imgUrl key:
      storage.ref('profile').child(imageAsFile.name).getDownloadURL()
       .then(fireBaseUrl => {
         setImageAsUrl(prevObject => ({...prevObject, imgUrl: fireBaseUrl}))
       })
    })
    }


  const handleSignUp = useCallback(async event => {
    event.preventDefault();
    const { email, password, name, image } = event.target.elements;
	
	var letters = /^[a-zA-Z\s]*$/;
	var emailregex = /(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*|"(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21\x23-\x5b\x5d-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])*")@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\[(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?|[a-z0-9-]*[a-z0-9]:(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21-\x5a\x53-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])+)\])/
	var passwordregex = /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{6,20}$/;
	
	try {
		var x = document.getElementById("name").value;
		var y = document.getElementById("email").value;
		var z = document.getElementById("password").value;
			if (x == "") {
    document.getElementById("demo").innerHTML = 'Emri është i domosdoshëm';
	return false
  }
		
	if(!x.match(letters))
      {
      document.getElementById("demo1").innerHTML = 'Kjo fushë pranon vetëm shkronja';
      return false;
      }	
	  
	  	if (y == "") {
    document.getElementById("demo2").innerHTML = 'Email është i domosdoshëm';
	return false
  }
	if(!y.match(emailregex))
      {
      document.getElementById("demo3").innerHTML = 'Rregullo email-in';
      return false;
      }
	  
	  if (z == "") {
    document.getElementById("demo4").innerHTML = 'Password është i domosdoshëm';
	return false
  }
	if(!z.match(passwordregex))
      {
      document.getElementById("demo5").innerHTML = 'Password between 6 to 20 characters which contain at least one numeric digit, one uppercase and one lowercase letter'
	  return false;
      }
	  
	  
      await app
        .auth()
        .createUserWithEmailAndPassword(email.value, password.value)
        .then(data => {
          data.user
            .updateProfile({
              displayName: name.value,
              photoURL : image.value
            })
            data.user.sendEmailVerification().then(function() {
  // Email sent.
}).catch(function(error) {
  // An error happened.
});
        })
      history.push("/");
    } catch (error) {
      alert(error);
    }
  }, [history]);
  
 const { currentUser } = useContext(AuthContext);

  if (currentUser) {
    return <Redirect to="/" />;
  }  


  return (
    <div className="main mt-5 mb-5">
    <p className="sign fontisign" align="center">Sign Up</p>
    <form className="form1" onSubmit={handleSignUp}>
   
      <img src={imageAsUrl.imgUrl || require("../assets/avatar.jpg")} className="signup-photo mb-4" alt="profile"/>
    <p className="text-danger text-center" id="demo"></p>
	<p className="text-danger text-center" id="demo1"></p>
    <input className="un " type="text"  placeholder="Emri" id="name" name="name"/>
	<p className="text-danger text-center" id="demo2"></p>
	<p className="text-danger text-center" id="demo3"></p>
     
      <input className="un " type="text"  placeholder="Email" id ="email" name="email"/>
	   <p className="text-danger text-center" id="demo4"></p>
	<p className="text-danger text-center pl-5 pr-5" id="demo5"></p>
	   <input className="pass" type="password" id="password" placeholder="Password" name="password"/>
      <input type="hidden"  name="image" value={imageAsUrl.imgUrl}/>
      <label style={{display:"block", textAlign:"center"} }>Foto e profilit</label>
      <input  type="file" className="pass" style={{width: "55%"}}  onChange={handleImageAsFile} />
    
        <button className="btn btn-success ml-1" style={{borderRadius:"3em", fontSize:"13px"}} onClick={handleFireBaseUpload}>Upload</button>
      <input type="submit" className="submit mb-5"  value="Sign Up"/>
      
            </form>
                
    </div>
  );
};

export default withRouter(SignUp);
