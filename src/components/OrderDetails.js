import axios from 'axios';
import { AuthContext } from "../Auth.js";
  import React from 'react';
  import { useContext } from "react";

  const MyOrders = ({match, product}) => {
    let params = match.params;
    const [repos, setRepos] = React.useState([]);
    const [repo, setRepo] = React.useState([]);
    const { currentUser } = useContext(AuthContext);
    
    React.useEffect(() => {
        
        const fetchData = async () => {
          const response = await axios.get('http://localhost:4000/orders');
          setRepos(response.data.filter(x => x._id === params.id));

          const responsee = await axios.get('http://localhost:4000/products');
          setRepo(responsee.data.filter(x => x._id === params.pid));
      }
      
      fetchData();
    }, [product]);
    

return (
    
  <div class="d-flex">
  <div class="container mb-5">
      <div >
    <h1 className="logo text-center mt-4 mb-4"> Lista e porosive </h1>
     <table class="col-md-12 mt-3 table table-striped table-dark tabela">
        <thead>
            <tr class="text-center">
            <th scope="col">Order ID</th>
            <th scope="col">Produkti</th>
            <th scope="col">Sasia</th>
            <th scope="col">Çmimi</th>
            <th scope="col">Totali</th>
            <th scope="col">Statusi</th>
            <th scope="col">Data</th>
            
            </tr>
        </thead>
        <tbody>
    
{ repos.map(items => (
 
            <tr class="text-center">

       
            <td>#{items._id}</td>   
            { repo.map(item => (
                    
            <td>{item.name}</td>
            
            ))}
         <td>{items.sasia}</td>
         { repo.map(item => (
             <td>{item.price}.00€</td>
         ))}
         { repo.map(item => (
             <td>{item.price * items.sasia}.00€</td>
         ))}
        
            <td>
            {(() => {
        if (items.status == 0) {
          return (
            <span className="text-warning">Në pritje</span>
          )
        } else if (items.status == 1) {
          return (
           <span className="text-success">Aprovuar</span>
          )
        } else {
          return (
            <span className="text-danger">Refuzuar</span>
          )
        }
      })()}</td>     
            <td>{items.updated_date}</td>
           
           
           </tr>
        

   
    
))}
</tbody>
    </table>

    { repo.map(item => (
            <td><img src={item.image}  className="img-fluid" /></td>
         ))}
</div></div></div>
)
}

export default MyOrders;