import React, {useEffect} from 'react'
import axios from 'axios'
import { useContext } from "react";
import { AuthContext } from "../Auth.js";
import {Link} from 'react-router-dom';
import {  Redirect } from "react-router";
import ReactGA from 'react-ga';


const ProductDetails = ({ match, product, lastproduct, category}) => {
	
		useEffect(() => {
    ReactGA.initialize('UA-172148803-1');
    // To Report Page View 
    ReactGA.pageview(window.location.pathname + window.location.search);
  }, [])
  const { currentUser } = useContext(AuthContext);
    let params = match.params;

    const [repos, setRepos] = React.useState([]);
    const [repo, setRepo] = React.useState([]);
    const [rep, setRep] = React.useState([]);
   
   React.useEffect(() => {
       const fetchData = async () => {
         const response = await axios.get('http://localhost:4000/products/');
         setRepos(response.data.filter(x => x._id === params.id  ));  

         const response1 = await axios.get('http://localhost:4000/products/');
         setRepo(response1.data.slice(-3));  

         const response2 = await axios.get('http://localhost:4000/category/');
         setRep(response2.data.filter(x => x._id === params.cat)); 
     }
     
     fetchData();
   }, [product, lastproduct, category]);
   
   var buybutton;
   if(currentUser && currentUser.emailVerified == true){

     buybutton = (
      <Link class="text-white" to = {`/buyform/${params.id}`}> <button type="button" className="btn btn-dark "> Bleje  <i className="cil-cart"></i></button></Link>
   )
    
 }
    return (
        <div className="col-md-11 mb-5 mt-5 mx-auto">
            { repos.map(item => (
		
				 <div>
            {(() => {
         if (item.isDeleted == false){
          return (
             <div class="row">
    <div className="col-md-6 mb-5">
    <img src={item.image} class="img-fluid" style={{ borderRadius:'20px'}} />
    </div>  
    <div class="col-md-5">
           
              <span style={{display:'inline-block', fontSize:'20px'}} class=" btn btn-warning text-white">{item.name}</span>
              <div className="float-right">
              <img src={item.flag} class="img-fluid mr-3" style={{height:'36px'}} />
              <span class="btn btn-primary mr-2">{item.price}.00€</span>
              {buybutton}
              </div>
              <hr></hr>
              { rep.map(item => ( <h3 className="btn btn-danger"><span style={{fontSize:'20px'}}> {item.name}</span></h3> ))}
                
                <div>
				
              <h4 className="mt-3 bg-success pl-3 pt-2 pb-2 pr-3 text-white" style={{display: 'inline-block', borderRadius:'5px', fontWeight:'normal'}}>Përshkrimi:</h4>
              <p style={{fontSize:'15px'}}> {item.description} </p>
              </div>
             

    </div>
    
    </div>
          )
        } else{
          return <Redirect to="/products" />;
        }
      })()}</div> 
		
		
  
    
       

            
    
))}

<hr></hr>
<h1 className="text-center logo mt-4" > Produktet e fundit</h1>
<div className="row d-flex m-3">
{ repo.map(item => (

		

   
    <div className="section m-3" style={{borderRadius:'20px', background:'none'}}>
    <img src={item.image} class="img-fluid" style={{width: '100%', height:'300px', marginBottom:'7px'}} />
    <Link class="text-danger" to = {`/productdetails/${item._id}`}><h4 className="text-center">{item.name}</h4></Link>
		
    
    </div>
)
)}</div>

        </div>
    )
}

export default ProductDetails;