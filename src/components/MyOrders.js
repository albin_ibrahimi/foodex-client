import axios from 'axios';
import { AuthContext } from "../Auth.js";
  import React from 'react';
  import { useContext, useState } from "react";
  import Pagination from './Pagination';
  import {Link} from 'react-router-dom';
  const MyOrders = ({orders, product}) => {
    const [repos, setRepos] = React.useState([]);
    const { currentUser } = useContext(AuthContext);
    const [currentPage, setCurrentPage] = useState(1);
    const [postsPerPage] = useState(5);
    React.useEffect(() => {
        
        const fetchData = async () => {
          const response = await axios.get('http://localhost:4000/orders');
          setRepos(response.data.filter(x => x.userid === currentUser.uid).reverse());


      }
      
      fetchData();
    }, [orders, product]);
    
       
	const indexOfLastPost = currentPage * postsPerPage;
  const indexOfFirstPost = indexOfLastPost - postsPerPage;
  const currentOrders = repos.slice(indexOfFirstPost, indexOfLastPost);

  // Change page
  const paginate = pageNumber => setCurrentPage(pageNumber);

return (
    
  <div class="d-flex">
  <div class="container mb-5">
      <div >
    <h1 className="logo text-center mt-4 mb-4"> Lista e porosive </h1>
     <table class="col-md-12 mt-3 table table-striped table-dark tabela">
        <thead>
            <tr class="text-center">
            <th scope="col">Order ID</th>
            <th scope="col">Statusi</th>
            <th scope="col">Data</th>
            
            </tr>
        </thead>
        <tbody>
    
{ currentOrders.map(item => (
 
            <tr class="text-center"  >
            <td><Link class="text-white" to = {`/orderdetails/${item._id}/${item.prodid}`}><button type="button" className="btn btn-outline-info ">#{item._id} <i className="cil-clipboard"></i></button></Link></td>   

            <td>
            {(() => {
        if (item.status == 0) {
          return (
            <span className="text-warning">Në pritje</span>
          )
        } else if (item.status == 1) {
          return (
           <span className="text-success">Aprovuar</span>
          )
        } else {
          return (
            <span className="text-danger">Refuzuar</span>
          )
        }
      })()}</td>     
            <td>{item.updated_date}</td>
           
           
           </tr>
        

   
    
))}
</tbody>
    </table>
</div>
<div className="d-flex justify-content-center">
<Pagination
        postsPerPage={postsPerPage}
        totalPosts={repos.length}
        paginate={paginate}
      />
</div></div>

</div>
)
}

export default MyOrders;