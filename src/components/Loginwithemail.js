import React, { useCallback, useContext } from "react";
import { withRouter, Redirect } from "react-router";
import app from "../base.js";
import { AuthContext } from "../Auth.js";
import { Link } from 'react-router-dom'; 

const Loginwithemail = ({ history }) => {
  const handleLogin = useCallback(
    async event => {
      event.preventDefault();
      const { email, password } = event.target.elements;
      try {
        await app
          .auth()
          .signInWithEmailAndPassword(email.value, password.value);
        history.push("/");
      } catch (error) {
        alert(error);
      }
    },
    [history]
  );

  const { currentUser } = useContext(AuthContext);

  if (currentUser) {
    return <Redirect to="/" />;
  }

  return (
    <div class="main mt-5 mb-5">
    <p class="sign" align="center">Login</p>
    <form class="form1" onSubmit={handleLogin}>
      <input class="un " type="text"  placeholder="Email" name="email"/>
      <input class="pass" type="password"  placeholder="Password" name="password"/>
      <input type="submit" class="submit"  value="Login"/>
      <p class="forgot pb-4" align="center"><Link to="/passwordreset" tag="li" active-class="active" exact><a className="nav-link">Forgot Password?</a></Link></p>
            </form>
                
    </div>
  );
};

export default withRouter(Loginwithemail);
