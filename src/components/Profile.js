import React from "react";
import { useContext, useEffect, useState } from "react";
import { AuthContext } from "../Auth.js";
import {Link} from 'react-router-dom';
import {app} from "../base";

const Profili = () => {
   
    const { currentUser } = useContext(AuthContext);
    const db = app.firestore();
    function submit() {

    

        db.collection("userdata").doc(currentUser.uid).set({
           adresa : document.getElementById("adresa").value,
           birthday :  document.getElementById("date").value,
           phonenumber :  document.getElementById("phone").value,
           userid : currentUser.uid
        })


    }
    function newsubmit() {
    db.collection("userdata").doc(currentUser.uid).update({
        adresa : document.getElementById("newadresa").value,
        birthday :  document.getElementById("newdate").value,
        phonenumber :  document.getElementById("newphone").value,
    })
    .then(function() {
        console.log("Document successfully updated!");
    });}

const [state, setState] = useState([]) 
    useEffect(() => {
        
        db.collection("userdata")
        .onSnapshot(function(querySnapshot) {
            let data = [];
            querySnapshot.forEach(function(doc) {
                data.push({birthday : doc.data().birthday,id : doc.data().userid, adresa : doc.data().adresa, phonenumber : doc.data().phonenumber,});
                setState(data.filter(x => x.id === currentUser.uid))
            });
        });
        // Should not ever set state during rendering, so do this in useEffect instead.
        
      }, []);

       var forma;
       var edit;
        if(state.length == 0){
                forma=(
                    <div className="pr-5 mt-4">
                    <h5 className="logo text-center">Shto të dhënat e tjera</h5>
                    <form onSubmit={submit}>

                                  <label for="family-name">Ditëlindja:</label>
                        <input type="date" id="date" className="form-control"/>
                      
                        <label for="city">Numri i telefonit:</label>
                        <input type="text" id="phone" className="form-control" />
                        <label for="population">Adresa:</label>
                        <input type="text" id="adresa" className="form-control"/>
                        <button className="btn btn-info mt-2 float-right" >Shto</button>
                                  </form></div>
                )

              
        }
        if(state.length != 0){
        edit=(
            <div className="edit-inputs mt-4">
                    <h5 className="logo text-center">Edito të dhënat</h5>
            <form onSubmit={newsubmit}>
            <div className="row">
    <input type="date" id="newdate" className="form-control col-md-4"/>

    < input type="text" id="newphone" className="form-control col-md-8" placeholder="Phone Number" />
    </div>
        <input type="text" id="newadresa" className="form-control mt-2" placeholder="Adresa"/>
<button className="btn btn-info mt-2 float-right" >Edito</button>
    </form></div>
        )}


   
  return (
      <div className="container">



          <h1 className="text-center logo mt-4 mb-5"> My Profile </h1>
          <div className=" mb-5 profile" style={{borderRadius:"15px", width:"80%", marginLeft:"10%"}}>
                        <div className="profile-image">
                        
                        <img src={currentUser.photoURL} className="img-fluid p-4 profile-photo"  />
                        </div>
                        <div className="profile-data">
                            <h6 style={{color:"#969997"}}>HELLO EVERYBODY, I AM </h6>
                            <h1>{currentUser.displayName}</h1>
                            <h6 className="mb-3">{currentUser.email}</h6>
                            { state.map(item => (
                                <div>
                                <h3><i class="fa fa-calendar" aria-hidden="true" style={{fontSize:"20px"}}></i><span style={{fontSize:"20px"}}>{item.birthday}</span></h3>
                                <h3><i class="fa fa-phone" aria-hidden="true" style={{fontSize:"20px"}}></i><span style={{fontSize:"20px"}}>{item.phonenumber}</span></h3>
                                <h1><i class="fa fa-home" aria-hidden="true" style={{fontSize:"20px"}}></i><span style={{fontSize:"20px"}}>{item.adresa}</span></h1>
                                </div>
                            ))}
                            
                           
                            
                            <Link class="text-dark" to = {`/myorders`}><h5><i class="fa fa-clipboard" aria-hidden="true" style={{fontSize:"20px"}}></i> My Orders</h5></Link>
                            {forma}
                            {edit}
                        </div>
                        </div>
  
                                   
                                    
                       
 
      </div>
  );
};

export default Profili;