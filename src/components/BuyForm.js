import React from 'react'
import axios from 'axios'
import { useContext, useState } from "react";
import { AuthContext } from "../Auth.js";
import {  Redirect } from "react-router";


const BuyForm = ({ match, product}) => {
  const { currentUser } = useContext(AuthContext);
  if(currentUser && currentUser.emailVerified == false){
    return <Redirect to="/products" />;
  }
    let params = match.params;
    const initialState = { prodid: params.id, name: currentUser.displayName, adresa: '', nrtelefonit: '' ,sasia:'', userid: currentUser.uid}
    const [article, setArticle] = useState(initialState) 
  
    function handleChange(event) { 
      setArticle({...article, [event.target.name]: event.target.value})
    }
  
    function handleSubmit(event) { 
      event.preventDefault();  
     
	  
	  var x = document.forms["myForm"]["adresa"].value;
	  var y = document.forms["myForm"]["nrtelefonit"].value;
	  var z = document.forms["myForm"]["sasia"].value;
	  var n = document.getElementById("sasia").value;
		var text;
 
  if (x == "") {
    document.getElementById("demo").innerHTML = 'Adresa është e domosdoshme';
	return false
  }
  
  if (y == "") {
    document.getElementById("demo1").innerHTML = 'Numri i telefonit është i domosdoshëm';
	return false
  }
  
  if (z == "") {
    document.getElementById("demo2").innerHTML = 'Sasia është e domosdoshme';
	return false
  }
  
  if (isNaN(n) || n < 1) {
    document.getElementById("demo3").innerHTML = 'Sasia pranon vetëm numra (1 e më shumë)';
	return false
  }
 
  
  
      async function postArticle() {
        try {
          const response = axios.post('http://localhost:4000/orders/', article); 
      
        } catch(error) {
          console.log('error', error);
        }
      }
      postArticle();
    }

    function OgKeyPress() {

        var sasia = document.getElementById("sasia");

        var s = sasia.value;


        var price = document.getElementById("price");

        var pprice = price.value


var totalamount = document.getElementById("totalamount");

        totalamount.value = 'Totali' +'-'+ Number(s) * Number(pprice) +'€';

    }

    const [repos, setRepos] = React.useState([]);
   
   React.useEffect(() => {
       const fetchData = async () => {
         const response = await axios.get('http://localhost:4000/products/');
         setRepos(response.data.filter(x => x._id === params.id  ));  

     }
     
     fetchData();
   }, [product]);
   

    
    return (
        <div className="mb-5 mt-5">
            <h1 className="text-center logo mb-5">Forma për blerjen e produktit</h1>
{ repos.map(item => (
                
   <div class="row">
      
    <div className="col-md-5 buy-image">
    <img src={item.image} class="img-fluid mb-4" style={{ borderRadius:'20px'}} />
    <span style={{display:'inline-block', fontSize:'18px'}} class=" btn btn-danger  text-white">{item.name}</span>
              <div className="float-right">
              <img src={item.flag} class="img-fluid mr-3" style={{height:'36px'}} />
              <span class="btn btn-primary">{item.price}.00€</span>
              </div>

    </div>  
    <div class="col-md-3 buy-form">
           
           
              <form onSubmit={handleSubmit} name="myForm">

<div className="form-group">
  <label>Adresa</label>
  <input name="adresa" type="text" value={article.adresa} onChange={handleChange} className="form-control" />
  <p class="text-danger" id="demo"></p>
</div>
<div className="form-group">
  <label>NrTelefonit</label>
  <input name="nrtelefonit" type="text" value={article.nrtelefonit} onChange={handleChange} className="form-control" />
	 <p class="text-danger" id="demo1"></p>
</div>
<div className="form-group">
  <label>Sasia</label>
  <input name="sasia" id="sasia" type="text" value={article.sasia} onChange={handleChange} onKeyPress={OgKeyPress} onKeyUp={OgKeyPress} className="form-control" />
  <p class="text-danger" id="demo2"></p>
  <p class="text-danger" id="demo3"></p>
  <input id="price" type="text" hidden value={item.price} /><br/>
 
</div>
<input id="totalamount" type="text" disabled className="totalamount bg-warning" />
  <input type="submit" value="Bleje" className="btn btn-success float-right" />

</form>
              
              
    </div>
    
    </div>
    
       

            
    
))}

<hr style={{width:"72%", marginLeft:"12%", marginTop:"30px"}}></hr>


        </div>
    )
}

export default BuyForm;