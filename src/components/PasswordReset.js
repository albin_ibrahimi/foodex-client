import React, { useCallback, useContext } from "react";
import { withRouter, Redirect } from "react-router";
import app from "../base";


const PasswordReset = ({ history }) => {


  
  const handleReset = useCallback(async event => {
    event.preventDefault();
    const { email } = event.target.elements;
    try {
      await app
        .auth()
        .sendPasswordResetEmail(email.value)
        .then(function() {
           alert("Linku për resetimin e fjalëkalimit u dërgua!")
          })
          .then(function() {
            history.push("/login");
           })
      
    } catch (error) {
      alert(error);
    }
  }, [history]);
  
 


  return (
    <div className="main mt-5 mb-5" style={{padding : "0 0 80px 0"}}>
    <p className="sign" align="center">Reset your password</p>
    <form className="form1" onSubmit={handleReset}>

      <input className="un " type="text"  placeholder="Email" name="email"/>
  
      <input type="submit" className="submit"  value="Reset"/>
      
            </form>
                
    </div>
  );
};

export default withRouter(PasswordReset);
