import axios from 'axios';
import { useHistory } from 'react-router-dom';
  import  React , { useState, useEffect} from 'react';
  import Pagination from './Pagination';
import {Link} from 'react-router-dom';
import ReactGA from 'react-ga';
import Slider from './Slider';

  const Products = ({product, match}) => {
    const history = useHistory();
    let params = match.params
  useEffect(() => {
    ReactGA.initialize('UA-172148803-1');
    // To Report Page View 
    ReactGA.pageview(window.location.pathname + window.location.search);
  }, [])
    const [repos, setRepos] = React.useState([]);
	 const [currentPage, setCurrentPage] = useState(1);
  const [postsPerPage] = useState(6);
  
    
    React.useEffect(() => {
        const fetchData = async () => {
          const response = await axios.get('http://localhost:4000/products');
          setRepos(response.data.filter(x => x.category == params.cid && x.isDeleted == false).sort(x => x.updated_date).reverse());
      }
      
      fetchData();
    }, [product]);
    
    function search(){
      history.push('/search/' + document.getElementById("search").value)
    }
   
	const indexOfLastPost = currentPage * postsPerPage;
  const indexOfFirstPost = indexOfLastPost - postsPerPage;
  const currentPosts = repos.slice(indexOfFirstPost, indexOfLastPost);

  // Change page
  const paginate = pageNumber => setCurrentPage(pageNumber);


return (
    
<div className="container">
 
   <Slider></Slider>
    <div className="col-md-4 d-flex mx-auto mb-5"> 
  <input type="text" id="search" className="form-control mr-2"></input>
  <button onClick={search} className="btn btn-success">Search</button></div>
{ currentPosts.map(item => (
  <div className="section  mb-5 ml-4">
    <div className="card">
    <img src={item.image} className=" rrite"/> 
    <hr/>
    <div className="pt-3">
    <h6 className="float-left pl-4">Emri: <span className="text-danger">{item.name}</span></h6>
    <h6 className=" float-right pr-4">Çmimi: <span className="text-danger">{item.price}.00€</span></h6>
	
 </div>
    <div className="card-footer mt-2">
    <div className="d-flex justify-content-between">
    <Link class="text-white" to = {`/productdetails/${item._id}/${item.category}`}><button type="button" className="btn btn-danger ">   Detajet <i className="cil-clipboard"></i></button></Link>
    </div>
    </div>
    </div>
    </div>   
    
))}
<div className="d-flex justify-content-center">
<Pagination
        postsPerPage={postsPerPage}
        totalPosts={repos.length}
        paginate={paginate}
      />
</div>
</div>
)
}

export default Products;