import React, {useEffect} from 'react';
import ReactGA from 'react-ga';

const About = () => {

  	useEffect(() => {
    ReactGA.initialize('UA-172148803-1');
    // To Report Page View 
    ReactGA.pageview(window.location.pathname + window.location.search);
  }, [])

  return (
    <div>
       <div id="prov2">
      <center>
        <div className="banner">
          <h1 className="display-4 text-white mt-5 mb-2">About us</h1>
          <h4 className="lead pb-2 text-white pl-3 pr-3"
          >"Të hash, është një domosdoshmëri. Të hash me zgjuarsi, është art."-(Jose Andrés)</h4>

          <h4 className="lead pb-2 text-white pl-3 pr-3">
            “Nuk ka nevojë të gatuash receta të zbukuruara ose komplekse, por ushqim të mirë nga përbërës të freskët."
            ."-(Mario Batali)
          </h4>
        </div>
      </center>
    </div>
    <div className="container">
      <div className="row">
        <div class="col-lg-8 col-sm-6 portfolio-item mt-5 mb-5 mx-auto">
          <div className="card">
            <div className="card-body">
              <h4 className="card-title">
                <br />
                <h2 className="text-center">📋</h2>
                <br />
              </h4>
              <div className="about text-center pb-5">
                <p>
                  Lakmoni disa ushqime të shijshme? Ndoshta ju jeni në humor për një biftek me lëng? Pavarësisht se çfarë lloj vakt keni në mendje, FOODEX është gati ta përgatisë atë për ju.
                  <br />
                  <br />Darka jonë shërben mëngjesin gjatë gjithë ditës, përveç opsioneve të shëndetshme dhe aromatike të darkës për drekë dhe darkë. Nga burgers te sallatat, ushqimet e detit e deri tek pastat, do të gjeni të gjitha llojet e vakteve të përzemërta të përgatitura të freskëta në Foodex.
                  <br />
                  <br />Darka jonë gjithashtu ka një furrë të plotë me produkte të shijshme të pjekura dhe trajta të tjera, përfshirë këtu edhe qumështin tonë të famshëm. Tingëllon e shijshme, apo jo?
                  Foodex është i hapur çdo ditë nga 8 e mëngjesit deri në 11 pasdite. Ne jemi këtu për t'ju shërbyer ushqimin më të mirë përreth.
                </p>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    </div>
  );
};

export default About;