import React from "react";
import { Link } from 'react-router-dom'; 
import app from "../base.js";
import { useContext } from "react";
import { AuthContext } from "../Auth.js";

  const Header = () => {

    var butoni;
    var emri;
   
    
	const { currentUser } = useContext(AuthContext);
	if(currentUser){
		
   
		
		butoni = (
    <ul class="navbar-nav ml-auto">
  
		<li className="nav-item nav-link" onClick={() => app.auth().signOut()} style={{cursor : "pointer"}}>Log out</li> 
    
    </ul>
  )
  emri = (<Link to="/profile" tag="li" active-class="active" exact style={{textDecoration:"none"}}><a tag="li" className="nav-link">Hi {currentUser.displayName}!</a></Link> )
    
}
  else{
    butoni = (
      <ul class="navbar-nav ml-auto">
        <li className="nav-item">
      <Link to="/login" tag="li" active-class="active" exact style={{textDecoration:"none"}}><a className="nav-link">Login</a></Link>
      </li>
      <li className="nav-item">
      <Link to="/signup" tag="li" active-class="active" exact style={{textDecoration:"none"}}><a className="nav-link">SignUp</a></Link>
      </li>
      </ul>
    )
   
  }

  function confirm() {
    currentUser.sendEmailVerification();
  }

  
  var confirmo;
  if(currentUser){
	    if(currentUser.emailVerified == false){
	  
	 
	  confirmo = (<h6 className = "confirm"> <button class="confirmbutton" onClick={confirm}>Confirm Email</button></h6>)
	
	 
  }
  }

  return (
    <div>
	
      <nav className="navbar navbar-expand-lg navbar-dark static-top font header">
      <div className="container">
      <a className="logo navbar-brand text-white" style={{fontSize:"24px" , padding:"0"}}>FOODEX </a>
      <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
        <span className="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarResponsive">
        <ul class="navbar-nav ml-auto">
          <li className="nav-item">
      <Link to="/" tag="li" active-class="active" exact style={{textDecoration:"none"}}><a className="nav-link">Home</a></Link>
      </li>
      <li className="nav-item">
      <Link to="/about" tag="li" active-class="active" exact style={{textDecoration:"none"}}><a className="nav-link">About</a></Link>
      </li>
	    <li className="nav-item">
      <Link to="/contact" tag="li" active-class="active" exact style={{textDecoration:"none"}}><a className="nav-link">Contact</a></Link>
      </li>
      <li className="nav-item">
      <Link to="/products" tag="li" active-class="active" exact style={{textDecoration:"none"}}><a className="nav-link">Products</a></Link>
      </li>
     
      
      
            {butoni}
            <li className="nav-item">
        {emri}
        
      </li> 
      </ul>
      </div>
      </div>
      </nav>
	 
	  {confirmo}
      </div>
    
  );
  }

  export default Header;