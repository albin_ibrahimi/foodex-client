import React from "react";

const Footer = () => {
  return (
    <div>
  
    <footer className="footer">
      <div className="rows">
        <div>
          <h1 className="footerlogo">FOODEX</h1>
        </div>

        <div className="ikonat">
          <a href="#" className="fa fa-facebook"></a>
          <a href="#" className="fa fa-instagram"></a>
          <a href="#" className="fa fa-twitter"></a>
          <a href="#" className="fa fa-google"></a>


          
        </div>

        <div className="mt-5 text-center">
          <p>
            Contact
            <br />
            <br />Dëshmorët e Kombit, Ferizaj
            <br />info@foodex.com
            <br />+ 383 45 123 456
            <br />+ 383 44 123 456
          </p>
        </div>
      </div>

      <div className="text-center pb-5">© 2020 Copyright: FOODEX</div>
    </footer>
  </div>
    
  );
};

export default Footer;