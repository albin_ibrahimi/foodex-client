import React, { useEffect } from "react";
import ReactGA from 'react-ga';

const Home = () => {

  useEffect(() => {
    ReactGA.initialize('UA-172148803-1');
    // To Report Page View 
    ReactGA.pageview(window.location.pathname + window.location.search);
  }, [])
  
  return (
    <div>
        <div id="prov">
      <center>
        <div className="banner">
          <h1>Fast and Tasty Food.</h1>
          <h4>Always fresh, never frozen.</h4>
        </div>
      </center>
    </div>
    <div className="container">
    <h1 className="text-center titulli fonti">Sistemi më i përshtatshëm për porosi online</h1>
    <div className="rreth">
        <div className="boss">
          <div class="res-circle">
            <img src={require('../assets/food1.png')} class="circle-txt" alt="ushqimi"/>
          </div>
          <p className="text-center pt-3">Ju eksploroni menynë</p>
        </div>
        <div className="boss ml-5">
          <div className="res-circle">
            <img src={require('../assets/food2.png')} className="circle-txt" alt="përgatitja"/>
          </div>
          <p className="text-center pt-3">Ne gatuajmë ushqimin tuaj</p>
        </div>
        <div className="boss ml-5">
          <div className="res-circle">
            <img src={require('../assets/food3.png')} className="circle-txt" alt="dërgimi"/>
          </div>
          <p className="text-center pt-3">Ne ua dërgojmë atë</p>
        </div>
      </div>
      <h1 class="text-center titulli mb-5 fonti">Recetat më të njohura</h1>
      <div className="row">
      {recetat.map(item => (
        <div className="section ml-3 card">
             <img src={item.image} className="img-fluid bigger receta-foto" />
          <p className="text-center pl-3 pr-3 pt-3 pb-1 receta-pershkrimi" >{item.description}</p>
          <hr />
          <h4 class="lead text-center text-danger pb-1">Shefi: {item.author}</h4>

        </div>
        
      ))}
    </div>
    </div>
    <div className="whois">
      <p className="text-white">
        <h4 className="shiko" >
          Shiko me shume!
          <br />Menyja e ushqimit përputhet mirë
          si: Meze të llojeve të ndryshme, me
          epitetin e krijuar
          si mezet më mira në sheher, byfteku, peshku i përga
          ditur në mënyren me perfekte
          dhe të gjitha recetat e
          që shoqërohen me sosin me të vëqantë “Babaganush”.
        </h4>
        <br />
        <br />
        <router-link to="/about">
          <button id="button-b" className="mr-4">About us</button>
        </router-link>

        <button id="button-b" >Read more</button>
      </p>
    </div>
    <div className="whois2">
      <div>
        <img src={require('../assets/back6.jpg')} className="whois2-img" />
      </div>
      <div className="pershkrimi float-left">
        <p className="ml-5 text-dark text-left mt-5 pl-4 pb-3">
          <span className="tekne"
          >Tek ne mund të ushqeheni shëndetshëm</span>
          <br />
          <br />
          <em className="sasia">
            Sasia e proteinave që çdo njeri duhet të marrë gjatë një
            dite ndryshon në varësi të peshës fizike dhe aktivitetit fizik,
            megjithatë specialistët thonë se gratë kanë nevojë për 40 deri
            në 75 gram proteina në ditë. Për të marrë sasinë e përditshme,
            çdo vakt duhet të përfshijë disa lloj proteinash, pa yndyrë dhe
            pa kolesterol. Produkte me pak yndyrë janë ato të qumështit,
            bishtajoret, fileto pule, peshku etj., janë zgjedhje të shëndetshme
            dhe me pak yndyrë.
          </em>
        </p>
      </div>
      <div className="spec">
        <img src={require('../assets/spec.png')} style={{width:"90%"}} className="speci-foto"/>
      </div>
    </div>
    </div>

  );
};

const recetat = [
  {
  description:"Përbërësit e njomë i hedhim te përbërësit e thatë dhe i trazojmë. Muffin me copëza çokollate duhet të jetë homogjene. Në fund shtojmë copëza çokollate dhe i përziejmë.",
  author: "Clarence Brown",
  image: "https://i.pinimg.com/564x/06/83/8a/06838ad26212942fb403a8d3ad5e749a.jpg"},
  {
    description:"Për të përgatitur mollë skuqur, hedhim vezë në një enë dhe e rrahim pak. Me pas shtojmë sheqerin dhe qumësht. Shtojmë miellin dhe e trazojmë mirë masën mos të ngelën kokrriza.. ",
    author: "Derek Sarno",
    image:"https://i.pinimg.com/564x/4c/61/84/4c6184e7af9787f35545b071e61212b6.jpg"
  },
  {
    description:"Në një tiganë me vaj, kaurdisim një lugë miell të cilin e shuajmë me ujë të ftohtë duke e trazuar shpejtë. Pasi merr valë në tiganë, e hedhim në tenxheren me mish..",
    author:"F. Scott Fitzgerald",
    image:"https://i.pinimg.com/564x/78/fc/5e/78fc5e0816f2e1a15947e51bb789b9a9.jpg"
  }


];

export default Home;