import React, { Component } from 'react';


class Contact extends Component {
 

  handleSubmit = (event) => {
    event.preventDefault();
    const data = {
      emri: event.target.emri.value,
      email: event.target.email.value,
      mesazhi: event.target.mesazhi.value,
    };
	
	var x = document.getElementById("emri").value;
	  var y = document.getElementById("email").value;
	  var z = document.getElementById("mesazhi").value;
		var letters = /^[a-zA-Z\s]*$/;
		var email = /(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*|"(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21\x23-\x5b\x5d-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])*")@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\[(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?|[a-z0-9-]*[a-z0-9]:(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21-\x5a\x53-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])+)\])/
		var message = /[a-zA-Z0-9~@#\^\$&\*\(\)-_\+=\,\.\?\s]*/;
		if (x == "") {
    document.getElementById("demo").innerHTML = 'Emri është i domosdoshëm';
	return false
  }
  if(!x.match(letters))
      {
      document.getElementById("demo3").innerHTML = 'Kjo fushë pranon vetëm shkronja';
      return false;
      }
  if (y == "") {
    document.getElementById("demo1").innerHTML = 'Email është i domosdoshëm';
	return false
  }
  if (!y.match(email)) {
    document.getElementById("demo4").innerHTML = 'Rregulloni email-in';
	return false
  }
  
   if (z == "") {
    document.getElementById("demo2").innerHTML = 'Mesazhi është i domosdoshëm';
	return false
  }
  
  if (!z.match(message)) {
    document.getElementById("demo5").innerHTML = 'Mos përdor karaktere speciale';
	return false
  }
  
  
	
    fetch("http://localhost:4000/messages/new", {
      method: 'POST',
      headers: { 'Content-Type': 'application/json' },
      body: JSON.stringify(data)
    })
	this.props.history.push('/');
  }

  render() {
    return (
<div className="d-flex">
      <div className="container features mb-5">
      <h1 className="mt-4 text-center mb-5 fonti">Contact</h1>
      <div className="row">
      <div className="col-lg-6 col-md-6 col-sm-12">
      <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2948.789600499279!2d21.089392014831812!3d42.34700894391941!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x135381e3278c1f3b%3A0x66b20731c7211569!2sRestaurant%20Peshku!5e0!3m2!1sen!2s!4v1589410447083!5m2!1sen!2s" width="100%" height="450" frameborder="0"  allowfullscreen=""></iframe>
    </div>
    <div className="col-lg-6 col-md-6 col-sm-12">
    <form onSubmit={event => this.handleSubmit(event)}>
    <div className="form-group">
    <input type="text" id="emri" name="emri" className="form-control" placeholder="Emri" />
	<p class="text-danger" id="demo"></p>
	<p class="text-danger" id="demo3"></p>
    </div>
    <div class="form-group">
    <input type="email" id="email" name="email" className="form-control" placeholder="Email" />
    <p class="text-danger" id="demo1"></p>
	<p class="text-danger" id="demo4"></p>
	</div>
    <div className="form-group">
    <textarea className="form-control" id="mesazhi" name="mesazhi" rows="4" placeholder="Mesazhi juaj..." ></textarea>
    <p class="text-danger" id="demo2"></p>
	<p class="text-danger" id="demo5"></p>
	</div>
    <input type="submit" className="btn btn-success btn-block" value="Dërgo 📩" /> 
    </form>




    </div>
        </div>
      </div>
    </div>

    );
  }
}

export default Contact;
