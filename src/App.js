import React, { useEffect } from "react";
import './assets/App.css' 
import 'bootstrap/dist/css/bootstrap.css';
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import Home from "./components/Home";
import About from "./components/About";
import Contact from "./components/Contact";
import Products from "./components/Products";
import MyOrders from "./components/MyOrders";
import OrderDetails from "./components/OrderDetails";
import Header from "./components/Header"
import Footer from './components/Footer'
import Loginwithemail from "./components/Loginwithemail";
import Login from "./components/Login";
import Profile from "./components/Profile";
import Search from "./components/Search";
import SignUp from "./components/SignUp";
import { AuthProvider } from "./Auth";
import PrivateRoute from "./PrivateRoute";
import PasswordReset from "./components/PasswordReset";
import ProductDetails from "./components/ProductDetails"
import BuyForm from "./components/BuyForm";
import Category from "./components/Category";

import ReactGA from 'react-ga';


const App = () => {
	
	useEffect(() => {
    ReactGA.initialize('UA-172148803-1');
    // To Report Page View 
    ReactGA.pageview(window.location.pathname + window.location.search);
  }, [])
  return (
    <AuthProvider>
      <Router>
        <div>
        <Header/>
            <Route exact path="/"  component={Home} /> 
            <Route exact path="/about"  component={About} />
          <Route exact path="/myorders" component={MyOrders} />
          <Route exact path="/search/:query" component={Search} />
          <Route exact path="/passwordreset" component={PasswordReset} />
		  <Route exact path="/contact" component={Contact} />
      <PrivateRoute exact path="/buyform/:id" component={BuyForm} />
      <PrivateRoute exact path="/orderdetails/:id/:pid" component={OrderDetails} />
          <Route exact path="/products" component={Products} />
          <Route exact path="/category/:cid" component={Category} />
		  <Route exact path="/productdetails/:id/:cat" component={ProductDetails} />
          <PrivateRoute exact path="/profile" component={Profile} />
          <Route exact path="/loginwemail" component={Loginwithemail} />
          <Route exact path="/login" component={Login} />
          <Route exact path="/signup" component={SignUp} />
          <Footer/>
        </div>
      </Router>
    </AuthProvider>
  );
};

export default App;
