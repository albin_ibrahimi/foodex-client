import firebase from "firebase/app";
import "firebase/storage";
import "firebase/auth";

// Your web app's Firebase configuration
const app = firebase.initializeApp({
  apiKey: "AIzaSyBGRvGq4zosW6EVdXMxQX-767FV4G3jNmw",
  authDomain: "libraria-89131.firebaseapp.com",
  databaseURL: "https://libraria-89131.firebaseio.com",
  projectId: "libraria-89131",
  storageBucket: "libraria-89131.appspot.com",
  messagingSenderId: "364752324957",
  appId: "1:364752324957:web:557d8b05545d3a6a1a40b4"
});

// Initialize Firebase


const storage = firebase.storage();

export { app,storage, firebase as default };
